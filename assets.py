import os
import urlparse
import re
import json
import scraperwiki
#from dshelpers import request_url
import requests_cache
import requests
import lxml.html
import logging
import hashlib
import warnings
requests_cache.install_cache('assets')
DIRECTORY = 'downloads'

class UnicodeFilenameError(Warning):
    pass

def get_asset_elements(root):
    """ Get asset elements for each ASSET_TAG
    >>> root = lxml.html.fromstring("<a href='cat'>dog<img src='pony'></a><a fail=true></a><img fail=true>")
    >>> [x[1] for x in get_asset_elements(root)]
    ['cat', 'pony']
    """

    # TODO TEST
    ASSETS = [{'path': './/a[@href]', 'attrib': 'href'},
              {'path': './/img[@src]', 'attrib': 'src'}]
    for item in ASSETS:
        elements = root.xpath(item['path'])
        for element in elements:
            url = element.attrib.get(item['attrib'])
            yield (element, element.attrib[item['attrib']])

def make_dir():
    try:
        os.mkdir(DIRECTORY)
    except OSError:  # directory exists
        pass

def get_original_filename_from_disposition(content_disposition):
    """
    Given a 'content-disposition' header, return the filename.
    Used by download_asset

    >>> get_original_filename_from_disposition('attachment; filename=genome.jpeg; modification-date="Wed, 12 Feb 1997 16:29:51 -0500"')
    u'genome.jpeg'
    >>> get_original_filename_from_disposition('inline;filename="1-1973 PH-MOA.pdf"')
    u'1-1973 PH-MOA.pdf'
    """
    filename = None
    disp = content_disposition.split(';')
    for part in disp:
        if part.strip().startswith('filename='):
            assert filename is None
            filename = part.partition('=')[2]
            if filename[0] == '"' and filename[-1] == '"':
                filename = filename[1:-1]  # remove quotes
    u_filename = filename.decode('ascii', 'ignore')

    if u_filename != filename:
        warnings.warn("unicode problem: {!r} != {!r}".format(u_filename, filename), UnicodeFilenameError)
    assert u_filename is not None
    assert len(u_filename) > 0
    assert '/' not in u_filename
    assert '\\' not in u_filename
    return u_filename

def download_asset(asset, dbsave=False):
    """ assets: dictionaries with original url of file ['original_url'] and the
                text of the link ['title'] (and an assetid)

    Download asset from original_url, save it to DOWNLOAD directory and
    save metadata (like filetype) to db
    """

    url = asset['original_url']
    title = asset['title']
    assetid = asset['assetid']
    subdir_name = hashlib.sha1(url).hexdigest()
    this_files_directory = os.path.join(DIRECTORY, subdir_name)
    try:
        os.mkdir(this_files_directory)
    except OSError:
        pass

    requests_cache.install_cache('assets')
    r = requests.get(url)

    logging.debug(r.headers)
    new_asset={}
    new_asset['original_url'] = url
    new_asset['title'] = title
    new_asset['assetid'] = assetid
    new_asset['content_type'] = r.headers.get('content-type').partition(';')[0]

    if r.headers.get('content-disposition'):
        new_asset['original_filename'] = get_original_filename_from_disposition(r.headers.get('content-disposition'))
    else:
        new_asset['original_filename'] = r.url.split('/')[-1].replace('/','')

    new_filename = new_asset['original_filename'].partition('?')[0] or '_'
    new_asset['filename'] = os.path.join(this_files_directory, new_filename)[:250]
    if dbsave:
        scraperwiki.sql.save(table_name='dl', data=new_asset, unique_keys=['original_url'])


    with open(new_asset['filename'], 'w') as f:
        f.write(r.content)
    
    return new_asset

def download_assets(assets):
    return [download_asset(asset) for asset in assets]

def all_assets(table='index'):
    """iterate over table to get all individual assets from all indexed docs"""
    assert False, 'all_assets used'
    sql = '_assets from "{}"'.format(table)
    for i, row in enumerate(scraperwiki.sql.select(sql)):
        if not row['_assets']:
            warnings.warn("skipped row {}".format(i))
            continue  # blank row
        j = json.loads(row['_assets'])
        for asset in j:
            yield asset

def get_asset_stubs(links):
    "Given a list of links, turn them into a json 'asset'"
    # NOTE: MUST iterate over same elements as replace_elements
    # to maintain referential integrity
    # links is a list of lxml elements
    urllist = [{"title": link.text_content() or link.attrib.get('alt') or 'PLACEHOLDER_TITLE',
                "original_url": link.attrib.get('href') or link.attrib.get('src'),
                "assetid": i}
               for i, link in enumerate(links)]
    return urllist

def _blank_link(_id='', link_format = '#ASSET{}', text='ASSET_TAG'):
    """
    Creates a link suitable for insertion into LXML element tree

    >>> lxml.html.tostring(_blank_link('123'))
    '<a href="#ASSET123">ASSET_TAG</a>'
    """
    e = lxml.html.Element('a')
    e.attrib['href']=link_format.format(_id)
    e.text=text
    return e

def _blank_img(_id='', link_format = '#ASSET{}', text='ASSET_TAG'):
    """
    Creates a image tag suitable for insertion into LXML element tree

    >>> lxml.html.tostring(_blank_img('123'))
    '<img src="#ASSET123">'
    """
    e = lxml.html.Element('img')
    e.attrib['src']=link_format.format(_id)
    e.attrib['alt']=text
    return e

def replace_elements(elements):
    """
    Replace all elements with blank elements

    >>> html = '<span><div><a href="nope">nope</a>yes<img src="kitten.jpg"></div></span>'
    >>> root = lxml.html.fromstring(html)
    >>> assets = root.xpath("//a")
    >>> assets.extend(root.xpath("//img"))
    >>> replace_elements(assets)
    >>> lxml.html.tostring(root)
    '<span><div><a href="#ASSET0">ASSET_TAG</a>yes<img src="#ASSET1"></div></span>'
    """
    # NOTE: MUST iterate over same elements as get_asset_stubs
    # to maintain referential integrity

    # NOTE: replace_elements MUST be after anything that needs an
    # unmangled URL - especailly get_asset_stubs. Or you could
    # deepcopy the HTML document.
    TAG_FUNCTIONS = {'a': _blank_link, 'img': _blank_img}
    for count, element in enumerate(elements):
        # want [ASSET_TAG](#ASSET_1)
        assert element.tag in TAG_FUNCTIONS
        blank = TAG_FUNCTIONS[element.tag](count)
        blank.tail = element.tail
        element.getparent().replace(element, blank)

def replace_element(element, text=''):
    blank = lxml.html.Element('span')
    blank.text = text
    blank.tail = element.tail
    element.getparent().replace(element, blank)

def get_assets(json_asset):
    """
    Retrieve previously saved asset information, given the asset
    information from the detail page only, in a form
    suitable for the final json file
    """
    assert False, 'get_assets used'
    unpacked_assets = json.loads(json_asset)
    output = []
    for asset in unpacked_assets:
        url = asset['original_url']
        try:
            row, = scraperwiki.sql.select("* from dl where original_url=?", [url])
        except ValueError as e:
            warnings.warn("Couldn't find DL for {}".format(url))
            warnings.warn(e)
            continue
        output.append(row)
    return output


make_dir()

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    download_asset({'original_url': "http://www.aaib.gov.uk/cms_resources/Dassault-Breguet Myst%C3%A8re-Falcon 900B, G-HMEV 07-08.pdf", 'title':'!!!', 'assetid':'123'})
    exit()
    #for asset in list(all_assets()):
    #    logging.info("downloading: {}".format(asset['original_url']))
    #    download_asset(asset)
