import requests
import lxml.html

import codecs
import sys
sys.stdout = codecs.getwriter('utf-8')(sys.stdout)

url = 'http://specialist-publisher.dev.gov.uk/drug-safety-updates'
 
def do_page(url):
    root = lxml.html.fromstring(requests.get(url).content)
    root.make_links_absolute(url)
    links = root.xpath("//li[@class='document']//a")
    for link in links:
        print
        print link.attrib['href']
        print link.text
 
    next = root.xpath("//a[@rel='next']/@href")
    if next:
        return next[0]
    else:
        exit()
 
while True:
    print "***", url
    url = do_page(url)
