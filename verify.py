import requests
import logging
from warnings import warn
import requests_cache

ALLCODES = range(0,1000)

class AssetVerificationError(Warning):
    pass

class LocalLink(AssetVerificationError):
    pass

class NotAWebLinkError(AssetVerificationError):
    pass

class BadAssetCodeError(AssetVerificationError):
    pass

class HTMLAssetError(AssetVerificationError):
    pass

class ConnectionError(AssetVerificationError):
    pass

class HorridURLError(AssetVerificationError):
    pass

class ImageTagContainsHTMLError(AssetVerificationError):
    pass

def verify(url, exception=True, source_url=None, tag=None):
    # TODO: do we need to remove the allowable codes hack?
    requests_cache.install_cache("verify", allowable_methods=('GET', 'HEAD'), allowable_codes=ALLCODES)
    if not exception:
        try:
            return verify(url, exception=True, source_url=source_url)
        except AssetVerificationError, e:
            warn("{}|{}|{}".format(url, e.__class__.__name__, e.message))
            return False

    # url isn't an internet link
    if not url.startswith('http'):
        warn(url.encode('utf-8'), NotAWebLinkError)
        return False
   
    if "//10." in url:
        warn(url.encode('utf-8'), LocalLink)
        return False

    # url is a 404 or similar
    try:
        r = requests.head(url, allow_redirects=True, timeout=20)
    except requests.exceptions.ConnectionError:
        warn(url, ConnectionError)
        return False
    except ValueError:
        warn(url.encode('utf-8'), HorridURLError)
        return False
    except Exception, e:
        warn('{}, {!r}'.format(url, e), AssetVerificationError)
        return False
    code = r.status_code
    logging.debug(code, url)
    try:
        r.raise_for_status()
    except Exception, e:
        warn('{}, {}'.format(repr(url), code), BadAssetCodeError)
        return False

    if tag == 'img':
        content_type = r.headers.get('content-type').partition(';')[0]
        if content_type == 'text/html':
            warn(url, ImageTagContainsHTMLError)
            return False
        return True
        
    # tag == 'a'
    offsite = 'mhra.gov.uk' not in url \
          and 'maib.gov.uk' not in url \
          and 'raib.gov.uk' not in url
   
    if offsite:
        return False

    # url is HTML *AND* from mhra.gov.uk
    content_type = r.headers.get('content-type').partition(';')[0]
    if content_type == 'text/html' and not(offsite):
        warn(url, HTMLAssetError)
        return False

    return True



if __name__ == '__main__':
    verify('https://google.com')
    print verify('http://www.emea.europa.eu/pdfs/human/opiniongen/Conventional_Antipsychotics_Article5.3-CHMP_Opinion.pdf', False)
    print verify('http://www.mhra.gov.uk/ConferencesLearningCentre/LearningCentre/Medicineslearningmodules/Antipsychoticslearningmodule/Antipsychotics%20learning%20module', False)
    print verify('http://www.mhra.gov.uk/Safetyinformation/DrugSafetyUpdate/ssLINK/CON081746', False)
    print verify('http://www.malaria-reference.co.uk/', False)
