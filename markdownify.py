import json
import html2text
import subprocess
import lxml.html
html2text.BODY_WIDTH = 0
html2text.SKIP_INTERNAL_LINKS = False
html2text.UNICODE_SNOB = 1

def asciinate(html):
    return html2text.html2text(html)

def _pandoc_process(options, content):
    command = ['/home/tool/gaptool/pandoc']
    command.extend(options)
    try:
        process = subprocess.Popen(command,
                                   stdin=subprocess.PIPE,
                                   stdout=subprocess.PIPE)
    except OSError:
        print "*** pandoc probably isn't executable. Run chmod +x pandoc ***"
        raise

    process.stdin.write(content)
    process.stdin.close()
    assert process.wait()==0
    return process.stdout.read()

def pandocify(html):
    options = ['-f', 'html',
               '-t', 'markdown_phpextra-raw_html',
               '--no-wrap']
    return _pandoc_process(options, html)

def pandocify_json(html):
    options = ['-f', 'html',
               '-t', 'json']
    return _pandoc_process(options, html)

def pandocify_json_out(json):
    options = ['-f', 'json',
               '-t', 'markdown_phpextra-raw_html',
               '--no-wrap']
    return _pandoc_process(options, json)

def roundtrip(html):
    r = lxml.html.tostring(lxml.html.fromstring(html))
    return r

if __name__ == '__main__':
    r = """
<html>
<table>
<tr><th>5<th>6<th>6</tr>
<tr><td>7<td>8<td>9</tr>
</table>

<b>5<br>6</b>
</html>
"""
    q = roundtrip(r)
    print q
    print pandocify(q)
    #print json.dumps(json.loads(pandocify_json(q)), indent=2)
