import os
import sys
import json
import re
import zipfile
from utf8csv import UnicodeWriter

def slugify(title):
    # stolen from https://github.com/alphagov/specialist-publisher/blob/master/app/lib/slug_generator.rb
    # TODO purge this abominable ****
    """title
      .downcase
      .gsub(/[^a-zA-Z0-9]+/, "-")
      .gsub(/-+$/, "")"""
    t = title.lower()
    t = re.sub('[^a-zA-Z0-9]+', '-', t)
    t = re.sub('-+$', '', t)
    return t

try:
    zip_filename, detail_filename, asset_filename = sys.argv[1:4]
except IndexError:
    print "usage: mapper <zipfile> <detail csv> <asset csv>"
    exit(1)

detail_csv = UnicodeWriter(open(detail_filename, 'w'))
asset_csv = UnicodeWriter(open(asset_filename, 'w'))
with zipfile.ZipFile(zip_filename) as z:
    for filename in z.namelist():
        if filename.startswith('metadata/') and filename.endswith('json'):
            with z.open(filename) as f:
                j = json.load(f)
                detail_csv.writerow( [slugify(j['title']),
                                      j['original_url']
                                     ] )
                for asset in j['_assets']:
                    asset_csv.writerow( [asset['original_url'],
                                         asset['original_filename'],
                                         asset['filename'],
                                        ] )
